import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.HashMap;

public class Task {

    private static final Logger log = Logger.getLogger(Task.class);


    private static HashMap<String, Byte> selectors = new HashMap<String, Byte>();
    private static ArrayList<String> urls = new ArrayList<String>();
    private static ArrayList<Parse> parsers = new ArrayList<Parse>();

    public Task() {
    }

    public HashMap<String, Byte> getSelectors() {
        return selectors;
    }

    public void setSelectors(HashMap<String, Byte> selectors) {
        Task.selectors = selectors;
    }

    public void addSelector(String query, Byte type){
        selectors.put(query, type);
    }

    public ArrayList<String> getUrls() {
        return urls;
    }

    public void setUrls(ArrayList<String> urls) {
        Task.urls = urls;
    }

    public void  addUrl(String url){
        urls.add(url);
    }

    public static ArrayList<Parse> getParsers() {
        return parsers;
    }

    public static void setParsers(ArrayList<Parse> parsers) {
        Task.parsers = parsers;
    }

    public static void addOneParser(String url){
        Parse parse = new Parse(url);
        parsers.add(parse);
    }

    public static Parse getOneParser(){
        for (Parse parser: parsers) {
            if (!parser.isBusy){
                return parser;
            }
        }
        return null;
    }

    public void findAllUrlsOnPage(Parse parse){
        log.info("Add all links from webpage");
       ArrayList<WebElement> elements = new ArrayList<WebElement>();
       elements.addAll(parse.driver.findElements(new By.ByLinkText("")));
       for (WebElement element : elements){
            String elem = element.getText();
            urls.add(elem);
            log.info(elem.toString());
        }

    }

}
