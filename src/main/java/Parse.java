import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

class Parse{

    private static final Logger log = Logger.getLogger(Parse.class);
    public static Integer waitTimeout;

    public WebDriver driver;
    public boolean isBusy = false;

    public Parse(){

    }

    public Parse(String url) {
       openWebDriver();
       openWebPage(url);

       this.waitTimeout =10;

    }

    public static Integer getWaitTimeout() {
        return waitTimeout;
    }

    public static void setWaitTimeout(Integer waitTimeout) {
        Parse.waitTimeout = waitTimeout;
    }

    public void openWebDriver() {
        isBusy = true;
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        this.driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        log.info("Driver created");
        isBusy = false;
    }

    public void closeWebDriver(){
        driver.close();
        driver.quit();
        isBusy = true;
    }

    public void openWebPage(String url){
        isBusy = true;
        this.driver.get(url);

        log.info("Page opened");
        isBusy = false;
    }

    public String getFullPageSource(){
        return driver.getPageSource();
    }

    public WebElement findElement(String query, byte type){
        isBusy = true;

        WebElement webElement = null;

        WebDriverWait wait = new WebDriverWait(driver, waitTimeout);

        try {
            switch (type) {
                case 1:
                    log.info("Use Tag Name search");
                    webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(new By.ByTagName(query)));
                    break;
                case 2:
                    log.info("Use search by ID");
                    webElement = wait.until(ExpectedConditions.visibilityOfElementLocated(new By.ById(query)));
                    break;
                case 3:
                    log.info("Use search by xPath");
                    webElement =  wait.until(ExpectedConditions.visibilityOfElementLocated(new By.ByXPath(query)));
                    break;
                case 4:
                    log.info("Use search by CssSelector");
                    webElement =  wait.until(ExpectedConditions.visibilityOfElementLocated(new By.ByCssSelector(query)));
                    break;
                default:
                    break;

                // must write another methods
            }
        } catch (TimeoutException e) {
            log.debug("Element not found, timeout or nothing on ");
        }
        if (webElement != null){
            log.info("Element is finded");
        }else {
            log.info("Nothing find -- query is bad");
        }
        isBusy = false;

        return webElement;
    }





}